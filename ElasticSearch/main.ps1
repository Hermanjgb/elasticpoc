terraform -version
terraform init

$env = "sbox"
$sub = ""
$template_id = "azure-compute-optimized" 
$ec_api_key = ""
$location = "westeurope"

terraform plan -var "backend_subscription_id=$sub" -var "ec_api_key=$ec_api_key" -var "location=$location" -var "env=$env" -var "elastic_search_template_id=$template_id"
terraform apply -var "backend_subscription_id=$sub" -var "ec_api_key=$ec_api_key" -var "location=$location" -var "env=$env" -var "elastic_search_template_id=$template_id"