resource "azurerm_resource_group" "monitor" {
  location = var.location
  name = "${var.env}-elastic-search"
}