# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/elastic/ec" {
  version     = "0.2.1"
  constraints = "0.2.1"
  hashes = [
    "h1:AHydnZSJn7IafZTDKIMwKHbp9m6XJ6uE5b0wcxzs4OA=",
    "zh:145414eaa5015fcad546eb17bd78a14720cadcfcca781a46c9a6493722eb3ba8",
    "zh:1cb7bacfe088c1145834e0a7d053f7aa7a8897a3554d040becad004774aa45f4",
    "zh:20e245ae46316853ad18d2eff9107aec3e0ffcaa5407b2aa422669d80a872761",
    "zh:4c9c52e4ee089d71a9f58f0f53173ded7a65012466dbb857b15e7697f25fdb40",
    "zh:52877b6e934e932aa27d85868e055a907172df9b0ca6bcc4a2c820cc6890b152",
    "zh:52cabbd5826ac10b5f4f9be25435f970ae0b9caed3cd3cdf42c71ee773c956ac",
    "zh:999963c9f6ee6d61f3aea2c76d0e842d963662f71b04b9cf4e003d88bd849d0e",
    "zh:9bd93ffef87de87b3f04bb8b572098ccb8d363a62a4c7fe43f74447ee4ee023f",
    "zh:a39bdff8f72d1479a9fa6a2399dab687bf775bcb7a1c4db0e5ef87fbbf66bfa9",
    "zh:aa3cd16eeae66ccaab372f3a6a819fe117516a4c96bc216663bc244983e11100",
    "zh:c5d3cc19dcf190bea741e75c4f90e1cda4673bef7a6b86f2b7f2eb1fd926dce0",
    "zh:fcdb180ef082876458ede8d1cdb2e703330120552c5d80268388d8437c9716fb",
    "zh:fd595f502fcd0ca8ada078114fbf5341880ac8e53493105c42309560654f3729",
  ]
}

provider "registry.terraform.io/hashicorp/azurerm" {
  version     = "2.77.0"
  constraints = ">= 2.47.0"
  hashes = [
    "h1:CrxU58snZUjswVXPIZkVA9zox8Y750oa+rUJFtMBMXk=",
    "zh:1d28e78fbd7846c081f2665bc45ec88d1be9de1253d2f6b78e60c3cb4bf1d066",
    "zh:1d792786118129b398a6ca7645e5b0bc77175e4056b92025e5d49aabb07b850e",
    "zh:34532bf0b48abc598a3137981daad3bc5a2c7aecb4c85d11ffa244188eb34eb7",
    "zh:34b67825a340f3d14036b69e2a9d78770f6a03ee1c984662f3d2e94440bba652",
    "zh:42de9e3f84960c951f4cda335920b63ce2fd1f9d1492ddea9ea2fa1756bb4cc0",
    "zh:763d65377957401cdf711ed29a43ec703a88155be06c147663e018589f91915c",
    "zh:89a18f26998c2794fc26dfdfb5467b1a4c652030af912d61dba8673ee4bb81e8",
    "zh:bd41a582a606cfaa6ea916be4a3a8fa7b0c291393605a534fcc612401aed30a1",
    "zh:c126ef57a507f5407b71aaa7bb2c5baab573ee05ec286c9d9acb3601b35e634c",
    "zh:de2856f03635a743d7e4ebd39eac52bd0ff357d0ae2398841332ea6e2b9a6754",
    "zh:ee7727ccadb4acb377d5a6a5cbfa56eda484ceaa91b4e54830044ffaeb638c06",
  ]
}
