terraform {
    backend "azurerm" {
        resource_group_name  = "sandbox-terraform" 
		storage_account_name = "terraformstatehglocal"
		container_name = "terraform-state"
		key            = "terraform.tfstate"
	}

	required_providers {
		azurerm = {
			 source = "hashicorp/azurerm"
             version = ">=2.47.0"
		}

		ec = {
			source  = "elastic/ec"
      		version = "0.2.1"
		}
	}
}

provider "azurerm" {
  features {}
  subscription_id = var.backend_subscription_id
}

provider "ec" {
  apikey  = var.ec_api_key
}