data "ec_stack" "latest" {
  version_regex = "latest"
  region        = "azure-${var.location}"
}

resource "ec_deployment" "elastic_search_resource" {
  # Optional name.
  name = "${var.env}-elastic-search"

  # Mandatory fields
  region                 = "azure-${var.location}"
  version                = data.ec_stack.latest.version
  deployment_template_id = var.elastic_search_template_id

  elasticsearch {}
}