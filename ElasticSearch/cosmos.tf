resource "azurerm_cosmosdb_account" "dxm_common" {
  location = var.location
  resource_group_name = azurerm_resource_group.monitor.name

  name = "${var.env}-dxm-cosmosdb"

  offer_type = "Standard"

  geo_location {
    failover_priority = 0
    location = var.location
  }

  consistency_policy {
    consistency_level = "Session"
    max_interval_in_seconds = 5
    max_staleness_prefix = 100
  }
}


resource "azurerm_cosmosdb_sql_database" "dxm_jobs_database" {
  account_name = azurerm_cosmosdb_account.dxm_common.name
  name  = "jobs"
  resource_group_name = azurerm_resource_group.monitor.name
  
  autoscale_settings {
	  max_throughput = 4000
  }
}

resource "azurerm_cosmosdb_sql_container" "dxm_jobs_event_store_container" {
  resource_group_name = azurerm_resource_group.monitor.name
  account_name = azurerm_cosmosdb_sql_database.dxm_jobs_database.account_name
  database_name = azurerm_cosmosdb_sql_database.dxm_jobs_database.name

  name = "JobsStatus"
  partition_key_path = "/jobId"
}