﻿using Elasticsearch.Net;
using ElasticSearchFunctionPOC;
using ElasticSearchFunctionPOC.Application;
using Microsoft.Azure.Functions.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using Nest;
using System;

[assembly: FunctionsStartup(typeof(Startup))]
namespace ElasticSearchFunctionPOC
{
    public class Startup : FunctionsStartup
    {
        public override void Configure(IFunctionsHostBuilder builder)
        {
            builder.Services.AddSingleton(CreateElasticClient)
                .AddSingleton<ServiceLogicRoot>()
                .AddSingleton(provider => ServiceLogicRootFrom(provider).query)
                .AddSingleton(provider => ServiceLogicRootFrom(provider).elasticQuery);
        }

        private static ElasticClient CreateElasticClient(IServiceProvider provider)
        {
            var elasticCloudId = Environment.GetEnvironmentVariable("ELASTIC_CLOUD_ID");
            var elasticUser = Environment.GetEnvironmentVariable("ELASTIC_USER");
            var elasticPassword = Environment.GetEnvironmentVariable("ELASTIC_PASSWORD");
            var settings = new ConnectionSettings(
                elasticCloudId, 
                new BasicAuthenticationCredentials(elasticUser, elasticPassword))
                .DefaultIndex("jobs");

            return new ElasticClient(settings);
        }

        private static ServiceLogicRoot ServiceLogicRootFrom(IServiceProvider provider)
        {
            return provider.GetRequiredService<ServiceLogicRoot>();
        }
    }
}
