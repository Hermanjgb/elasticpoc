﻿using ElasticSearchFunctionPOC.Application.Providers;
using ElasticSearchFunctionPOC.Ports;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticSearchFunctionPOC.Application.Factories
{
    public class TokenProviderFactory : ITokenProviderFactory
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
       

        public TokenProviderFactory(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public ITokenProvider CreateForHttpContext()
        {
            return new HttpContextTokenProvider(_httpContextAccessor);
        }
    }
}
