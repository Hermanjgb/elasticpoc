﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ElasticSearchFunctionPOC.Application.Extensions
{
    public static class UrlExtensions
    {
        public static string ExtractFileId(this string uploadPath)
        {
            var segments = uploadPath.Split("?");
            return string.Join("/", segments[0].Split(@"/").Skip(4));
        }
    }
}
