﻿using ElasticSearchFunctionPOC.Ports;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchFunctionPOC.Application.Providers
{
    public class HttpContextTokenProvider : ITokenProvider
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public HttpContextTokenProvider(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public Task<string> GetAccessToken()
        {
            try
            {
                var userToken = _httpContextAccessor.HttpContext.Request.Headers["Authorization"].ToString().Replace("Bearer", "");

                if (string.IsNullOrWhiteSpace(userToken))
                {
                    throw new Exception("Cannot retreive token from http context.");
                }

                return Task.FromResult(userToken);
            }
            catch (Exception ex)
            {
                throw new Exception("Cannot retreive token from http context.", ex);
            }
        }
    }
}
