﻿using ElasticSearchFunctionPOC.Adapters;
using ElasticSearchFunctionPOC.Adapters.Cosmos.Queries;
using ElasticSearchFunctionPOC.Adapters.Cosmos.Repos;
using ElasticSearchFunctionPOC.Application.Factories;
using ElasticSearchFunctionPOC.Ports.Dto;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace ElasticSearchFunctionPOC.Application
{
    public class ServiceLogicRoot
    {
        public MultipleJobsQuery query { get; }
        public ElasticMultipleJobsQuery elasticQuery { get; }
        public ServiceLogicRoot() 
        {

            var jobStatusStoreInit = JobStatusStorageInitializer.CreateFor(
                "JobsStatus",
                "AccountEndpoint=https://sbox-dxm-cosmosdb.documents.azure.com:443/;AccountKey=jeSYMWBq5zz9C8lR89bDbFaJtrid6fnpS4GDgtKWRmwCaphQLqR9C34Jo1KXgqvAZgtO6IK3CCNVlD9SvT5Vow==;",
                "jobs");

            var jobStatusRepository = new JobStatusRepository(jobStatusStoreInit);
            query = new MultipleJobsQuery(jobStatusRepository,  new JobDtoFactory());
            elasticQuery = new ElasticMultipleJobsQuery(new JobDtoFactory());
        }
    }
}
