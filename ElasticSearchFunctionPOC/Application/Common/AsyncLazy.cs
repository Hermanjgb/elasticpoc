﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ElasticSearchFunctionPOC.Application.Common
{
    public sealed class AsyncLazy<T>
        where T : class
    {
        private readonly Func<CancellationToken, Task<T>> _valueFactory;
        private readonly SemaphoreSlim _mutex;

        private T? _value;

        public AsyncLazy(Func<CancellationToken, Task<T>> valueFactory)
        {
            _valueFactory = valueFactory;
            _mutex = new SemaphoreSlim(1, 1);
        }

        public async Task<T> GetValueAsync(CancellationToken cancellationToken)
        {
            await _mutex.WaitAsync(cancellationToken);
            try
            {
                if (_value == null)
                {
                    _value = await _valueFactory(cancellationToken).ConfigureAwait(false);
                    cancellationToken.ThrowIfCancellationRequested();
                }
                return _value;
            }
            finally
            {
                _mutex.Release();
            }
        }
    }
}
