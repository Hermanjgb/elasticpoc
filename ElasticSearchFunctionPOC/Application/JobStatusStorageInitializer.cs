﻿using ElasticSearchFunctionPOC.Application.Common;
using Microsoft.Azure.Cosmos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ElasticSearchFunctionPOC.Application
{
    public class JobStatusStorageInitializer
    {
        private readonly AsyncLazy<Container> _storageInitializer;
        private const string PartitionKey = "/jobId";

        private JobStatusStorageInitializer(string storageName, string cosmosDbConnectionString, string databaseName)
        {
            _storageInitializer =
                new AsyncLazy<Container>(token => CreateEventStoreAsync(storageName, cosmosDbConnectionString, databaseName, token));
        }

        public static JobStatusStorageInitializer CreateFor(string storageName, string cosmosDbConnectionString, string databaseName)
        {
            return new JobStatusStorageInitializer(storageName, cosmosDbConnectionString, databaseName);
        }

        public Task<Container> GetStoreAsync(CancellationToken cancellationToken)
        {
            return _storageInitializer.GetValueAsync(cancellationToken);
        }

        private static async Task<Container> CreateEventStoreAsync(string storageName, string cosmosDbConnectionString, string databaseName, CancellationToken cancellationToken)
        {
            var cosmosClientOption = new CosmosClientOptions
            {
                AllowBulkExecution = true
            };

            var client = new CosmosClient(cosmosDbConnectionString, cosmosClientOption);
            var databaseResponse = await client.CreateDatabaseIfNotExistsAsync(databaseName, cancellationToken: cancellationToken);
            var container = await databaseResponse.Database.CreateContainerIfNotExistsAsync(
                new ContainerProperties(storageName, PartitionKey), cancellationToken: cancellationToken);

            return container.Container;
        }
    }
}
