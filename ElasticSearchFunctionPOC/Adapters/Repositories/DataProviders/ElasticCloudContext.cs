﻿using ElasticSearchFunctionPOC.Ports;
using Nest;
using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticSearchFunctionPOC.Adapters.Repositories.DataProviders
{
    public class ElasticCloudContext : IElasticCloudContext
    {
        private readonly ElasticClient _client;

        public ElasticCloudContext(ElasticClient client) 
        {
            _client = client;
        }

    }
}
