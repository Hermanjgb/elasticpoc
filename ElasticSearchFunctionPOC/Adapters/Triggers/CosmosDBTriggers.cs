using System.Collections.Generic;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;

namespace ElasticSearchFunctionPOC.Adapters.Triggers
{
    public static class CosmosDBTriggers
    {
        [FunctionName("Function1")]
        public static void Run([CosmosDBTrigger(
            databaseName: "jobs",
            collectionName: "JobsStatus",
            ConnectionStringSetting = "cosmosConnectionString",
            CreateLeaseCollectionIfNotExists = true)]IReadOnlyList<object> input, ILogger log)
        {
            if (input != null && input.Count > 0)
            {
                log.LogInformation("Documents modified " + input.Count);
                //log.LogInformation("First document Id " + input[0].Id);
            }
        }
    }
}
