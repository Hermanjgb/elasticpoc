﻿using ElasticSearchFunctionPOC.Application.Extensions;
using ElasticSearchFunctionPOC.Ports;
using ElasticSearchFunctionPOC.Ports.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ElasticSearchFunctionPOC.Adapters.Cosmos.Queries
{
    public class JobDtoFactory : IJobDtoFactory
    {
        public JobDtoFactory()
        {}

        public async Task<JobDto> FromAsync(Guid jobId,
            JobStatus jobStatus,
            IEnumerable<SiteJobStatus> sitesJobStatuses, CancellationToken cancellationToken)
        {
            var statusesList = sitesJobStatuses.ToList();
            var siteStatuses = await statusesList.ToAsyncEnumerable()
                .SelectAwait(async s => new DetailedJobSummaryDto(
                    s.SiteId,
                    JobOverallStatus.Get(s.DeviceJobsStatuses),
                    await s.DeviceJobsStatuses.ToAsyncEnumerable()
                        .SelectAwait(async d => new DetailedDeviceJobSummaryDto(
                            d.DeviceId,
                            d.UpdatedAt,
                            d.Details,
                            d.Status,
                            d.ErrorCode,
                            null,
                            d.History
                        )).ToListAsync(cancellationToken)
                )).ToListAsync(cancellationToken);

            return new JobDto(
                jobStatus.JobId,
                jobStatus.Name,
                jobStatus.Note,
                jobStatus.CreatedAt,
                jobStatus.CreatedBy,
                jobStatus.Scheduled,
                JobOverallStatus.Get(statusesList),
                new JobActionDto(
                    jobStatus.JobAction.Name,
                    jobStatus.JobAction.EdgeId,
                    jobStatus.JobAction.ModuleId,
                    jobStatus.JobAction.Fields.Select(f => new ActionFieldDto(f.Name, f.Value))
                ),
                siteStatuses,
                jobStatus.UserGroups,
                jobStatus.DeviceTypes,
                jobStatus.History);
        }
    }
}
