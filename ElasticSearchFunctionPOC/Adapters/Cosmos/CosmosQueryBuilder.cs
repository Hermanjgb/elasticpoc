﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ElasticSearchFunctionPOC.Adapters.Cosmos
{
    public class CosmosQueryBuilder
    {
        private StringBuilder _queryBuilder;

        private CosmosQueryBuilder()
        {
            _queryBuilder = new StringBuilder();
        }

        public static CosmosQueryBuilder Create()
        {
            return new CosmosQueryBuilder();
        }

        public CosmosQueryBuilder SelectAll()
        {
            _queryBuilder.Append("SELECT * FROM c");
            return this;
        }

        public CosmosQueryBuilder Select(params string[] elements)
        {
            _queryBuilder.Append($"SELECT {string.Join(", ", elements)} FROM c");
            return this;
        }

        public CosmosQueryBuilder CountAll()
        {
            _queryBuilder.Append("SELECT COUNT(1) FROM c");
            return this;
        }

        public CosmosQueryBuilder SelectDistinctValue(string field)
        {
            if (!string.IsNullOrEmpty(field))
                _queryBuilder.Append($"SELECT DISTINCT VALUE c.{field} FROM c");

            return this;
        }

        public CosmosQueryBuilder Where()
        {
            _queryBuilder.Append(" WHERE ");
            return this;
        }

        public CosmosQueryBuilder And()
        {
            var query = _queryBuilder.ToString().TrimEnd();

            if (query.EndsWith("AND") || query.EndsWith("WHERE"))
            {
                return this;

            }

            _queryBuilder.Append(" AND ");
            return this;
        }

        public CosmosQueryBuilder Or()
        {
            var query = _queryBuilder.ToString().TrimEnd();

            if (query.EndsWith("OR") || query.EndsWith("WHERE"))
            {
                return this;
            }

            _queryBuilder.Append(" OR ");
            return this;
        }

        public CosmosQueryBuilder WithQueryPart(string query)
        {
            _queryBuilder.Append($" {query} ");
            return this;
        }

        public CosmosQueryBuilder Parameter(string parameterPath, string parameterValue, string comparisonSign = "=", bool checkIfEmpty = true)
        {
            if (checkIfEmpty && string.IsNullOrWhiteSpace(parameterValue))
            {
                return this;
            }

            _queryBuilder.Append($"c.{parameterPath} {comparisonSign} '{parameterValue}'");
            return this;
        }

        public CosmosQueryBuilder In<TValueType>(string parameterPath, IEnumerable<TValueType> paramaterValues, bool checkIfEmpty = true)
        {
            var conditionValues = string.Join(',', paramaterValues.Select(g => $"'{g}'"));

            if (checkIfEmpty && string.IsNullOrWhiteSpace(conditionValues))
            {
                return this;
            }

            _queryBuilder.Append($"c.{parameterPath} IN ({conditionValues})");
            return this;
        }

        public CosmosQueryBuilder ArrayContains(string arrayPath, string objectName, string objectValue, bool checkIfEmpty = true)
        {
            if (checkIfEmpty && string.IsNullOrWhiteSpace(objectValue))
            {
                return this;
            }

            _queryBuilder.Append($"ARRAY_CONTAINS(c.{arrayPath}, {{\"{objectName}\": \"{objectValue}\"}}, true)");
            return this;
        }

        public CosmosQueryBuilder ArrayContains<TItem>(string fieldName, IEnumerable<TItem>? items)
        {
            if (items != null && items.Any())
                _queryBuilder.Append($"({string.Join(" OR ", items.Select(item => $"ARRAY_CONTAINS(c.{fieldName}, '{item}')"))})");

            return this;
        }

        public CosmosQueryBuilder MultipleOr<TItem>(string fieldName, IEnumerable<TItem>? items)
        {
            if (items != null && items.Any())
                _queryBuilder.Append($"({string.Join(" OR ", items.Select(item => $"c.{fieldName} = '{item}'"))})");

            return this;
        }

        public CosmosQueryBuilder WithSearchText(string searchText, IEnumerable<string>? searchFields)
        {
            if (!string.IsNullOrEmpty(searchText) && searchFields != null && searchFields.Any())
            {
                _queryBuilder.Append($"({string.Join(" OR ", searchFields.Select(field => $"CONTAINS(LOWER(c.{field}),'{searchText.ToLower()}')"))})");
            }
            return this;
        }

        public CosmosQueryBuilder WithOffset(int elementsPerPage, int currentPage)
        {
            if (elementsPerPage != default && currentPage != default)
            {
                var offset = elementsPerPage * (currentPage - 1);
                _queryBuilder.Append($" OFFSET {offset} LIMIT {elementsPerPage} ");
            }

            return this;
        }

        public CosmosQueryBuilder OrderBy(string parameterPath, string order)
        {
            if (!string.IsNullOrEmpty(parameterPath))
                _queryBuilder.Append($" ORDER BY c.{parameterPath} {order} ");

            return this;
        }

        public string Build()
        {
            var queryDefinition = _queryBuilder.ToString();

            if (queryDefinition.TrimEnd().EndsWith("WHERE"))
            {
                return queryDefinition.Replace("WHERE", "");
            }

            return _queryBuilder.ToString();
        }
    }
}
