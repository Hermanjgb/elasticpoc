﻿using System.Threading;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Text;
using ElasticSearchFunctionPOC.Ports.Dto;
using Nest;
using System.Linq;
using ElasticSearchFunctionPOC.Ports;

namespace ElasticSearchFunctionPOC.Adapters.Cosmos.Repos
{
    public class ElasticMultipleJobsQuery
    {

        private readonly IJobDtoFactory _jobDtoFactory;

        public ElasticMultipleJobsQuery(IJobDtoFactory jobDtoFactory)
        {
            _jobDtoFactory = jobDtoFactory;
        }

        public async Task<JobStatusWithPagingDto<DetailedJobSummaryDto>> ExecuteAsync(JobFilter jobFilter, ElasticClient client, CancellationToken cancellationToken)
        {
            var shortJobStatuses = new List<JobDto>();
            var jobOverallStatusRequestKey = "jobStatus";
            var jobStatusesCount = 0;
            IEnumerable<Guid> authorizedUserGroups = new List<Guid> {
                   new Guid("a18943db-7236-4338-9e2f-76d05bd13d60"),
                   new Guid("e964a9f8-cb3c-44e7-81af-f4b0062a47f0"),
                   new Guid("e2f69741-8fdf-476c-b038-f951bdbd6cfb"),
                   new Guid("afa1d55b-66c5-42a0-acdc-797458d9bb66"),
                   new Guid("0931639a-5554-4cf8-a11f-46f00948ad41"),
                   new Guid("e968a74c-6d2f-442e-a38a-80380e3cadb5"),
                   new Guid("0bf55dcb-1f18-4be3-9290-d340e0491cf7"),
                   new Guid("1924d860-78ca-4f2e-a8ac-9bf773a0504d"),
                   new Guid("bbfbc0b8-a24d-43dd-8a4b-40385a58020d"),
                   new Guid("d0cd3146-0dcf-4d1f-8890-7566de5d194e"),
                   new Guid("91984a1d-a083-493a-8a99-9a03c14ed9d4"),
                   new Guid("2f49e179-f970-4ac5-b653-4ad5f35f1773"),
                   new Guid("62fba411-25a4-45d8-9e20-b80b0f3a3775"),
                   new Guid("6be6ef3c-be30-4b22-ae43-6d3388a47cc0"),
                   new Guid("260ae852-7370-45be-88ca-17211db5ef0b"),
                   new Guid("6210e1c4-8911-40a3-b7fa-8d70c5931cc9")
            };

            var siteJobStatues =  await client.SearchAsync<SiteJobStatus>(s => s.Query(q => q.Match(m => m.Field(f => f.Type).Query("SiteJobStatus"))), cancellationToken);
            
            if (!siteJobStatues.Hits.Any())
                return null;

            var authorizedSites = siteJobStatues.Hits.Select(s => s.Source.SiteId).Distinct();
            var authSites = siteJobStatues.Hits.Where(s => authorizedSites.Contains(s.Source.SiteId));


            //var jobStatues = await client.SearchAsync<JobStatus>(s =>
            //s.From(jobFilter.CurrentPage - 1).Size(jobFilter.ItemsPerPage).Query(q => q.Match(m => m.Field(f => f.Type).Query("jobStatus"))

            //), cancellationToken);

            var jobStatues = await client.SearchAsync<JobStatus>(s => s
                .From(jobFilter.CurrentPage - 1)
                .Size(jobFilter.ItemsPerPage)
                .Query(q => q.Bool(bq => 
                    bq.Must(mu => mu.Match(m => m.Field(f => f.Type).Query("jobStatus")))
                    .Should(s => TermAny(s, "JobId", authSites.Select(x => x.Source.JobId.ToString()).Distinct().ToArray()))
                    .Should(f => TermAny(f, "UserGroups", authorizedUserGroups.Select(id => id.ToString()).ToArray()))
                )), 
                cancellationToken);



            foreach (var jobStatus in jobStatues.Hits)
            {
                var siteStatuses = siteJobStatues.Hits.Where(s => s.Source.JobId == jobStatus.Source.JobId).Select( s => s.Source);

                if (jobFilter.DeviceId != null)
                {
                    siteStatuses = siteStatuses.Select(s => s.FilteredBy(jobFilter.DeviceId));
                }

                var shortJobStatus = await _jobDtoFactory.FromAsync(jobStatus.Source.JobId, jobStatus.Source, siteStatuses, cancellationToken);

                if (jobFilter.JobStatuses != null && jobFilter.JobStatuses.Any())
                {
                    if (jobFilter.JobStatuses.Contains(shortJobStatus.OverallStatus)) //Filter by Job statuses
                        shortJobStatuses.Add(shortJobStatus);
                }
                else
                    shortJobStatuses.Add(shortJobStatus);
            }

            if (jobFilter.OrderBy == jobOverallStatusRequestKey)
            {
                shortJobStatuses = (jobFilter.IsDescendingOrder)
                    ? shortJobStatuses.OrderByDescending(j => j.OverallStatus.ToString()).ToList()
                    : shortJobStatuses.OrderBy(j => j.OverallStatus.ToString()).ToList();
            }

            if (jobFilter.JobStatuses != null && jobFilter.JobStatuses.Any())
            {
                jobStatusesCount = shortJobStatuses.Count;

                shortJobStatuses = shortJobStatuses
                    .Skip(jobFilter.ItemsPerPage * (jobFilter.CurrentPage - 1))
                    .Take(jobFilter.ItemsPerPage)
                    .ToList();
            }

            return new JobStatusWithPagingDto<DetailedJobSummaryDto>(shortJobStatuses, Convert.ToInt32(jobStatues.Total), jobFilter.CurrentPage, jobFilter.ItemsPerPage);
        }

        private static QueryContainer TermAny<T>(QueryContainerDescriptor<T> descriptor, Field field, object[] values) where T : class
        {
            QueryContainer q = new QueryContainer();
            foreach (var value in values)
            {
                q |= descriptor.Term(t => t.Field(field).Value(value));
            }
            return q;
        }
    }
}
