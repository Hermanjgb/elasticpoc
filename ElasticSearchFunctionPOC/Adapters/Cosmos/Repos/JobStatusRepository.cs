﻿using ElasticSearchFunctionPOC.Application;
using ElasticSearchFunctionPOC.Ports;
using ElasticSearchFunctionPOC.Ports.Dto;
using Microsoft.Azure.Cosmos;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ElasticSearchFunctionPOC.Adapters.Cosmos.Repos
{
    public class JobStatusRepository : IJobStatusRepository
    {
        private readonly JobStatusStorageInitializer _jobStatusStoreInit;

        public JobStatusRepository(JobStatusStorageInitializer jobStatusStoreInit)
        {
            _jobStatusStoreInit = jobStatusStoreInit;
        }
      

        public Task<IEnumerable<JobStatus>> GetJobStatusAsync(IEnumerable<Guid> jobIds, CancellationToken cancellationToken)
        {
            return GetJobStatusAsync(jobIds, string.Empty, null, null, null, null, string.Empty, null, string.Empty, false, default, default, string.Empty, null, cancellationToken);
        }

        public async Task<JobStatus> GetJobStatusAsync(Guid jobIds, CancellationToken cancellationToken)
        {
            var jobStatus = await GetJobStatusAsync(new[] { jobIds }, cancellationToken);
            return jobStatus.FirstOrDefault();
        }

        public async Task<IEnumerable<SiteJobStatus>> GetPagedSitesJobsStatusesFilteredByAsync(string siteId,
            Guid? deviceId, DateTime? createdAfter, CancellationToken cancellationToken)
        {
            var queryDefinition = CosmosQueryBuilder.Create()
                                    .SelectAll()
                                    .Where()
                                    .Parameter("siteId", siteId)
                                    .AndFilterBy(deviceId)
                                    .And()
                                    .Parameter("createdAt", createdAfter?.ToString("O"), comparisonSign: ">")
                                    .And()
                                    .Parameter("type", nameof(SiteJobStatus))
                                    .OrderBy("createdAt", "DESC")
                                    .Build();

            var feedIterator = (await _jobStatusStoreInit.GetStoreAsync(cancellationToken)).GetItemQueryIterator<SiteJobStatus>(queryDefinition, requestOptions: new QueryRequestOptions
            {
                MaxItemCount = -1
            });

            var siteJobsStatueses = new List<SiteJobStatus>();

            while (feedIterator.HasMoreResults)
            {
                var response = await feedIterator.ReadNextAsync(cancellationToken);

                foreach (var siteJobStatus in response)
                {
                    siteJobsStatueses.Add(siteJobStatus);
                }
            }

            return siteJobsStatueses;
        }

        public async Task<IEnumerable<JobStatus>> GetJobStatusAsync(IEnumerable<Guid> jobIds, string moduleId, IEnumerable<string>? jobActions,
                                                        DateTime? scheduledAfter, DateTime? scheduledBefore,
                                                        Guid? createdBy, string jobName, IEnumerable<string>? deviceTypes,
                                                        string orderBy, bool isDesc, int elementsPerPage, int currentPage,
                                                        string searchText, IEnumerable<Guid>? authorizedUserGroups, CancellationToken cancellationToken)
        {
            var queryDefinition = CosmosQueryBuilder.Create()
                                    .SelectAll()
                                    .Where()
                                    .In("jobId", jobIds)
                                    .And()
                                    .Parameter("jobAction.moduleId", moduleId)
                                    .And()
                                    .MultipleOr("jobAction.name", jobActions)
                                    .And()
                                    .Parameter("scheduledDate", scheduledAfter?.ToString("O"), comparisonSign: ">=")
                                    .And()
                                    .Parameter("scheduledDate", scheduledBefore?.ToString("O"), comparisonSign: "<=")
                                    .And()
                                    .Parameter("createdBy", createdBy?.ToString())
                                    .And()
                                    .Parameter("name", jobName)
                                    .And()
                                    .ArrayContains("deviceTypes", deviceTypes)
                                    .And()
                                    .ArrayContains("userGroups", authorizedUserGroups)
                                    .And()
                                    .WithSearchText(searchText, new List<string>() { "name", "jobId" })
                                    .And()
                                    .Parameter("type", nameof(JobStatus))
                                    .OrderBy(orderBy, isDesc ? "DESC" : string.Empty)
                                    .WithOffset(elementsPerPage, currentPage)
                                    .Build();

            var feedIterator = (await _jobStatusStoreInit.GetStoreAsync(cancellationToken)).GetItemQueryIterator<JobStatus>(queryDefinition, requestOptions: new QueryRequestOptions
            {
                MaxItemCount = -1
            });

            var jobsStatuses = new List<JobStatus>();

            while (feedIterator.HasMoreResults)
            {
                var response = await feedIterator.ReadNextAsync(cancellationToken);

                foreach (var siteJobStatus in response)
                {
                    jobsStatuses.Add(siteJobStatus);
                }
            }

            return jobsStatuses;
        }

        public async Task<int> CountJobStatusAsync(IEnumerable<Guid> jobIds, string moduleId, IEnumerable<string>? jobActions,
                                                        DateTime? scheduledAfter, DateTime? scheduledBefore,
                                                        Guid? createdBy, string jobName, IEnumerable<string>? deviceTypes,
                                                        string searchText, IEnumerable<Guid>? authorizedUserGroups, CancellationToken cancellationToken)
        {
            var queryDefinition = CosmosQueryBuilder.Create()
                                        .CountAll()
                                        .Where()
                                        .In("jobId", jobIds)
                                        .And()
                                        .Parameter("jobAction.moduleId", moduleId)
                                        .And()
                                        .MultipleOr("jobAction.name", jobActions)
                                        .And()
                                        .Parameter("scheduledDate", scheduledAfter?.ToString("O"), comparisonSign: ">=")
                                        .And()
                                        .Parameter("scheduledDate", scheduledBefore?.ToString("O"), comparisonSign: "<=")
                                        .And()
                                        .Parameter("createdBy", createdBy?.ToString())
                                        .And()
                                        .Parameter("name", jobName)
                                        .And()
                                        .ArrayContains("deviceTypes", deviceTypes)
                                        .And()
                                        .ArrayContains("userGroups", authorizedUserGroups)
                                        .And()
                                        .WithSearchText(searchText, new List<string>() { "name", "jobId" })
                                        .And()
                                        .Parameter("type", nameof(JobStatus))
                                        .Build();

            var feedIterator = (await _jobStatusStoreInit.GetStoreAsync(cancellationToken)).GetItemQueryIterator<JToken>(
                queryDefinition,
                requestOptions: new QueryRequestOptions
                {
                    MaxItemCount = -1
                });

            var response = await feedIterator.ReadNextAsync(cancellationToken);
            return response.Resource.First().Value<int>("$1");
        }

        public async Task<List<string>> GetDeviceTypes(CancellationToken cancellationToken)
        {
            var queryDefinition = CosmosQueryBuilder.Create()
                                    .SelectDistinctValue("deviceTypes")
                                    .Where()
                                    .Parameter("type", nameof(JobStatus))
                                    .Build();

            var feedIterator = (await _jobStatusStoreInit.GetStoreAsync(cancellationToken)).GetItemQueryIterator<List<string>>(queryDefinition, requestOptions: new QueryRequestOptions
            {
                MaxItemCount = -1
            });

            var deviceTypes = new List<List<string>>();

            while (feedIterator.HasMoreResults)
            {
                var response = await feedIterator.ReadNextAsync(cancellationToken);

                foreach (var item in response)
                {
                    if (item != null && item.Any())
                        deviceTypes.Add(item);
                }
            }

            if (!deviceTypes.Any())
                return new List<string>();

            return deviceTypes.SelectMany(l => l).Distinct().ToList();
        }

        public async Task<List<string>> GetJobActions(CancellationToken cancellationToken)
        {
            var queryDefinition = CosmosQueryBuilder.Create()
                                    .SelectDistinctValue("jobAction.name")
                                    .Where()
                                    .Parameter("type", nameof(JobStatus))
                                    .Build();

            var feedIterator = (await _jobStatusStoreInit.GetStoreAsync(cancellationToken)).GetItemQueryIterator<string>(queryDefinition, requestOptions: new QueryRequestOptions
            {
                MaxItemCount = -1
            });

            var jobActions = new List<string>();

            while (feedIterator.HasMoreResults)
            {
                var response = await feedIterator.ReadNextAsync(cancellationToken);

                foreach (var item in response)
                {
                    if (!string.IsNullOrEmpty(item))
                        jobActions.Add(item);
                }
            }

            if (!jobActions.Any())
                return new List<string>();

            return jobActions.Distinct().ToList();
        }
    }

    public static class CosmosQueryBuilderEx
    {
        public static CosmosQueryBuilder AndFilterBy(this CosmosQueryBuilder builder, Guid? deviceId) =>
            deviceId == null
                ? builder
                : builder.And().ArrayContains("deviceJobsStatuses", "deviceId", deviceId.ToString());
    }
}
