﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using System.Reflection;
using System.Threading.Tasks;

namespace ElasticSearchFunctionPOC.Adapters.Processor
{
    public static class VersionController
    {
        [FunctionName("version")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = null)] HttpRequest req,
            ILogger log)
        {
            return new OkObjectResult(GetVersion());
        }


        private static string GetVersion()
        {
            string version;

            try
            {
                version = Assembly.GetExecutingAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion;
            }
            catch
            {
                version = "Not Available";
            }

            return version;
        }
    }
}
