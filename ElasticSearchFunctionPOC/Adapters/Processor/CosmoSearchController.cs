﻿using ElasticSearchFunctionPOC.Adapters.Cosmos.Queries;
using ElasticSearchFunctionPOC.Ports.Dto;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Nest;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ElasticSearchFunctionPOC.Adapters.Processor
{
    public class CosmoSearchController
    {
        private readonly MultipleJobsQuery _query;
        private readonly ElasticClient _client;

        public CosmoSearchController(MultipleJobsQuery query, ElasticClient client) 
        {
            _query = query;
            _client = client;
        }

        [FunctionName(nameof(RunGetAllCosmosAsync))]
        public async Task<IActionResult> RunGetAllCosmosAsync(
           [HttpTrigger(AuthorizationLevel.Function, "get", Route = "cosmos")]
            HttpRequest request,
           CancellationToken cancellationToken)
        {
            var res = await _query.ExecuteAsync(
                JobFilter.Create(), _client, cancellationToken);

            return new OkObjectResult(res);
        }
    }
}
