﻿using ElasticSearchFunctionPOC.Adapters.Cosmos.Repos;
using ElasticSearchFunctionPOC.Ports.Dto;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Nest;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ElasticSearchFunctionPOC.Adapters.Processor
{
    public class ElasticSearchController
    {
        private const string FUNCTION_NAME = "search";
        private const string FUNCTION_PARAMERTER = "search/{id}";
        private readonly ElasticClient _client;
        private readonly ElasticMultipleJobsQuery _query;

        public ElasticSearchController(ElasticClient client, ElasticMultipleJobsQuery query) 
        {
            _client = client;
            _query = query;
        }

        [FunctionName(nameof(RunGetAllAsync))]
        public async Task<IActionResult> RunGetAllAsync(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = FUNCTION_NAME)]
            HttpRequest request,
            CancellationToken cancellationToken)
        {
            var test = await _query.ExecuteAsync(JobFilter.Create(), _client, cancellationToken);

            return new OkObjectResult(test);
        }

        [FunctionName(nameof(CreateSearchObject))]
        public async Task<IActionResult> CreateSearchObject(
          [HttpTrigger(AuthorizationLevel.Function, "post", Route = FUNCTION_NAME)] HttpRequest requestt,
            CancellationToken cancellationToken)
        {
            try
            {
                var response = await _client.IndexDocumentAsync(new Job2Dto()
                {
                    Id = Guid.NewGuid(),
                    Name = $"test-{Guid.NewGuid()}"
                }, cancellationToken);

                return new OkResult();
            }
            catch (Exception ex) 
            {
                return new BadRequestObjectResult(ex.Message);
            }
            
        }

        [FunctionName(nameof(RunGetAsync))]
        public async Task<IActionResult> RunGetAsync(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = FUNCTION_PARAMERTER)]
            HttpRequest request,
            Guid id,
            CancellationToken cancellationToken)
        {
            var test = await _client.SearchAsync<Job2Dto>(s => s
                        .From(0)
                        .Size(10).Query(q => q
                        .Match(m => m
                        .Field(f => f.Id)
                        .Query(id.ToString()))
            ), cancellationToken);

            return new OkObjectResult(test.Hits);
        }

        [FunctionName(nameof(DeleteSearchObject))]
        public async Task<IActionResult> DeleteSearchObject(
          [HttpTrigger(AuthorizationLevel.Function, "delete", Route = FUNCTION_PARAMERTER)] HttpRequest requestt, 
            Guid id,
            CancellationToken cancellationToken)
        {
            try
            {
                var response = await _client.DeleteAsync(DocumentPath<Job2Dto>.Id(id.ToString()), ct: cancellationToken);

                return new OkObjectResult(response);
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(ex.Message);
            }

        }


        [FunctionName(nameof(PingSearch))]
        public async Task<IActionResult> PingSearch(
           [HttpTrigger(AuthorizationLevel.Function, "get", Route = "Ping")] HttpRequest request)
        {
            var response = await _client.PingAsync();

            return new OkObjectResult(response.DebugInformation);
        }
    }
}
