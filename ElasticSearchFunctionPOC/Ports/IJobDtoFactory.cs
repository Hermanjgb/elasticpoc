﻿using ElasticSearchFunctionPOC.Ports.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ElasticSearchFunctionPOC.Ports
{
    public interface IJobDtoFactory
    {
        Task<JobDto> FromAsync(Guid jobId,
            JobStatus jobStatus,
            IEnumerable<SiteJobStatus> sitesJobStatuses, CancellationToken cancellationToken);
    }
}
