﻿using Flurl;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ElasticSearchFunctionPOC.Ports
{
    public interface IUriProvider
    {
        Task<Url> ForAsync(Guid jobId, Guid deviceId, string fileId, CancellationToken cancellationToken);
    }
}
