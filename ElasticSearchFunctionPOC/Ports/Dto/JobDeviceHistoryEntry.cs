﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticSearchFunctionPOC.Ports.Dto
{
    public class JobDeviceHistoryEntry
    {
        public JobDeviceHistoryEntry(JobStatusType status, DateTime timestamp, string details)
        {
            Status = status;
            Timestamp = timestamp;
            Details = details;
        }

        [JsonProperty("status")]
        [JsonConverter(typeof(StringEnumConverter))]
        public JobStatusType Status { get; set; }

        [JsonProperty("timestamp")]
        public DateTime Timestamp { get; set; }

        [JsonProperty("details")]
        public string Details { get; set; }
    }
}
