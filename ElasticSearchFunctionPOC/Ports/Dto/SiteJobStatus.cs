﻿using Microsoft.Azure.Documents;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ElasticSearchFunctionPOC.Ports.Dto
{
    public class SiteJobStatus : Resource
    {
        [JsonProperty("siteId")]
        public Guid SiteId { get; set; }

        [JsonProperty("jobId")]
        public Guid JobId { get; set; }

        [JsonProperty("deviceJobsStatuses")]
        public IList<DeviceJobStatus> DeviceJobsStatuses { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }

        public SiteJobStatus FilteredBy(Guid? deviceId)
        {
            return new SiteJobStatus
            {
                SiteId = SiteId,
                JobId = JobId,
                CreatedAt = CreatedAt,
                DeviceJobsStatuses = DeviceJobsStatuses.Where(x => x.DeviceId == deviceId).ToList(),
                Id = Id
            };
        }
    }
}
