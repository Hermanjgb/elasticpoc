﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticSearchFunctionPOC.Ports.Dto
{
    public class ManifestActionFieldDto
    {
        [JsonProperty("converter", NullValueHandling = NullValueHandling.Ignore)]
        public string? Converter { get; set; }

        [JsonProperty("name")]
        public string? Name { get; set; }

        [JsonProperty("type", NullValueHandling = NullValueHandling.Ignore)]
        public string? Type { get; set; }
    }
}
