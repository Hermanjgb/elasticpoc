﻿using Microsoft.Azure.Documents;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticSearchFunctionPOC.Ports.Dto
{
    public class JobStatus : Resource
    {
        [JsonProperty("jobId")]
        public Guid JobId { get; set; }

        [JsonProperty("note")]
        public string Note { get; set; }

        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("createdBy")]
        public Guid CreatedBy { get; set; }

        [JsonProperty("userGroups")]
        public IEnumerable<Guid> UserGroups { get; set; }

        [JsonProperty("jobAction")]
        public JobAction JobAction { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("scheduled")]
        public ScheduleTimeDto? Scheduled { get; set; }

        [JsonProperty("scheduledDate")]
        public DateTime? ScheduledDate { get; set; }

        [JsonProperty("deviceTypes")]
        public IEnumerable<string> DeviceTypes { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("history")]
        public IEnumerable<JobHistoryEntry> History { get; set; }
    }
}
