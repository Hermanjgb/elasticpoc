﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticSearchFunctionPOC.Ports.Dto
{
    public enum JobActionType
    {
        New = 0,
        Updated = 1,
        Cancelled = 2
    }
}
