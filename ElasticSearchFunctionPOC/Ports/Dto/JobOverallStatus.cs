﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ElasticSearchFunctionPOC.Ports.Dto
{
    public class JobOverallStatus
    {
        public static JobStatusType Get(IEnumerable<DeviceJobStatus> deviceJobStatuses)
        {
            return Get(deviceJobStatuses.Select(s => s.Status));
        }

        public static JobStatusType Get(IEnumerable<SiteJobStatus> deviceJobStatuses)
        {
            var devicesStatuses = new List<JobStatusType>();
            deviceJobStatuses.ToList().ForEach(s =>
                devicesStatuses.AddRange(s.DeviceJobsStatuses.Select(d => d.Status)));

            return Get(devicesStatuses);
        }

        private static JobStatusType Get(IEnumerable<JobStatusType> deviceJobStatuses)
        {
            var jobStatusTypes = deviceJobStatuses as JobStatusType[] ?? deviceJobStatuses.ToArray();
            if (IsNew(jobStatusTypes))
                return JobStatusType.New;
            if (IsCancellationPending(jobStatusTypes))
                return JobStatusType.Cancelling;
            if (IsCancelled(jobStatusTypes))
                return JobStatusType.Cancelled;
            if (IsScheduled(jobStatusTypes))
                return JobStatusType.Scheduled;
            if (IsCompleted(jobStatusTypes))
                return JobStatusType.Completed;
            if (IsCompletedWithErrors(jobStatusTypes))
                return JobStatusType.CompletedWithErrors;

            return JobStatusType.Running;
        }

        private static bool IsCancelled(IEnumerable<JobStatusType> deviceJobStatuses)
        {
            return deviceJobStatuses.Any(s => s == JobStatusType.Cancelled);
        }

        private static bool IsCancellationPending(IEnumerable<JobStatusType> deviceJobStatuses)
        {
            return deviceJobStatuses.Any(s => s == JobStatusType.Cancelling);
        }

        private static bool IsCompletedWithErrors(IEnumerable<JobStatusType> deviceJobStatuses)
        {
            return deviceJobStatuses.All(s => s == JobStatusType.Completed || s == JobStatusType.Failed);
        }

        private static bool IsCompleted(IEnumerable<JobStatusType> deviceJobStatuses)
        {
            return deviceJobStatuses.All(s => s == JobStatusType.Completed);
        }

        private static bool IsNew(IEnumerable<JobStatusType> deviceJobStatuses)
        {
            return !deviceJobStatuses.Any() || deviceJobStatuses.All(s => s == JobStatusType.New);
        }

        private static bool IsScheduled(IEnumerable<JobStatusType> deviceJobStatuses)
        {
            return deviceJobStatuses.All(s => s == JobStatusType.Scheduled);
        }
    }
}
