﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticSearchFunctionPOC.Ports.Dto
{
    public class UploadedFileDto
    {
        [JsonProperty(PropertyName = "fieldName")]
        public string FieldName { get; }

        [JsonProperty(PropertyName = "uri")]
        public Uri Uri { get; }

        [JsonConstructor]
        public UploadedFileDto(string fieldName, Uri uri)
        {
            FieldName = fieldName;
            Uri = uri;
        }
    }
}
