﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticSearchFunctionPOC.Ports.Dto
{
    public class DetailedJobSummaryDto
    {
        public DetailedJobSummaryDto(
            Guid siteId,
            JobStatusType siteJobStatus,
            IEnumerable<DetailedDeviceJobSummaryDto> devices)
        {
            SiteId = siteId;
            SiteJobStatus = siteJobStatus;
            Devices = devices;
        }

        [JsonProperty("siteId")]
        public Guid SiteId { get; }

        [JsonProperty("siteJobStatus")]
        [JsonConverter(typeof(StringEnumConverter))]
        public JobStatusType SiteJobStatus { get; }

        [JsonProperty("devices")]
        public IEnumerable<DetailedDeviceJobSummaryDto> Devices { get; }
    }
}
