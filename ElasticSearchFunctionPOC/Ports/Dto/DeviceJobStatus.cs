﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticSearchFunctionPOC.Ports.Dto
{
    public class DeviceJobStatus
    {
        [JsonProperty("deviceId")]
        public Guid DeviceId { get; set; }

        [JsonProperty("status")]
        [JsonConverter(typeof(StringEnumConverter))]
        public JobStatusType Status { get; set; }

        [JsonProperty("details")]
        public string Details { get; set; }

        [JsonProperty("errorCode", NullValueHandling = NullValueHandling.Ignore)]
        public int? ErrorCode { get; set; }

        [JsonProperty("updatedAt")]
        public DateTime UpdatedAt { get; set; }

        [JsonProperty("invocationDetails")]
        public InvocationDetailsDto? InvocationDetails { get; set; }

        [JsonProperty("history")]
        public IEnumerable<JobDeviceHistoryEntry> History { get; set; }
    }
}
