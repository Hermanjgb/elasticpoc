﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticSearchFunctionPOC.Ports.Dto
{
    public class SiteDto
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        public SiteDto(Guid id)
        {
            Id = id;
        }
    }
}
