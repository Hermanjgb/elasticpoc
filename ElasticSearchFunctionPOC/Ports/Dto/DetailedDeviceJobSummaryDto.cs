﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticSearchFunctionPOC.Ports.Dto
{
    public class DetailedDeviceJobSummaryDto
    {

        public DetailedDeviceJobSummaryDto(
            Guid deviceId,
            DateTime statusUpdatedAt,
            string details,
            JobStatusType status,
            int? errorCode,
            IList<UploadedFileDto>? uploadedFiles,
            IEnumerable<JobDeviceHistoryEntry> history)
        {
            DeviceId = deviceId;
            StatusUpdatedAt = statusUpdatedAt;
            Details = details;
            Status = status;
            ErrorCode = errorCode;
            UploadedFiles = uploadedFiles;
            History = history;
        }

        [JsonProperty("uploadedFiles")]
        public IList<UploadedFileDto>? UploadedFiles { get; }

        [JsonProperty("deviceId")]
        public Guid DeviceId { get; }

        [JsonProperty("statusUpdatedAt")]
        public DateTime StatusUpdatedAt { get; }

        [JsonProperty("status")]
        [JsonConverter(typeof(StringEnumConverter))]
        public JobStatusType Status { get; }

        [JsonProperty("details")]
        public string Details { get; }

        [JsonProperty("errorCode", NullValueHandling = NullValueHandling.Ignore)]
        public int? ErrorCode { get; }

        [JsonProperty("history")]
        public IEnumerable<JobDeviceHistoryEntry> History { get; }
    }
}
