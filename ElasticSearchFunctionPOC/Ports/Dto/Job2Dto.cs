﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticSearchFunctionPOC.Ports.Dto
{
    public class Job2Dto
    {
        public Guid Id { get; set; }
        
        [JsonProperty("jobId")]
        public Guid JobId { get; set; }
        public string Name { get; set; }
    }
}
