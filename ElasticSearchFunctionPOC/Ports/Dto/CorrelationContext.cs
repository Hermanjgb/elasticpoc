﻿using Newtonsoft.Json;

namespace ElasticSearchFunctionPOC.Ports.Dto
{
    public class CorrelationContext
    {
        public DeviceJobId DeviceJobId { get; }
        public string ActivityId { get; }

        [JsonConstructor]
        public CorrelationContext(DeviceJobId deviceJobId, string activityId)
        {
            DeviceJobId = deviceJobId;
            ActivityId = activityId;
        }
    }
}
