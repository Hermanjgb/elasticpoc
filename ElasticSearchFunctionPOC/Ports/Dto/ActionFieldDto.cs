﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticSearchFunctionPOC.Ports.Dto
{
    public class ActionFieldDto
    {
        [JsonProperty("name")]
        public string Name { get; }

        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("doNotOverride")]
        public bool DoNotOverride { get; }

        public ActionFieldDto(string name, string value, bool doNotOverride = false)
        {
            Name = name;
            Value = value;
            DoNotOverride = doNotOverride;
        }
    }
}
