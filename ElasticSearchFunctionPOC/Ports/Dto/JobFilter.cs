﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticSearchFunctionPOC.Ports.Dto
{
    public class JobFilter
    {
        public JobFilter(
            string siteId,
            Guid? deviceId,
            string moduleId,
            IEnumerable<string>? jobActions,
            int itemsPerPage,
            int currentPage,
            DateTime? createdAfter,
            Guid? createdBy,
            IEnumerable<JobStatusType>? jobStatuses,
            string jobName,
            IEnumerable<string>? deviceTypes,
            DateTime? scheduledAfter,
            DateTime? scheduledBefore,
            string orderBy,
            bool isDescendingOrder,
            string search
            )
        {
            SiteId = siteId;
            DeviceId = deviceId;
            ModuleId = moduleId;
            JobActions = jobActions;
            ItemsPerPage = itemsPerPage;
            CurrentPage = currentPage;
            CreatedAfter = createdAfter;
            CreatedBy = createdBy;
            JobStatuses = jobStatuses;
            JobName = jobName;
            DeviceTypes = deviceTypes;
            ScheduledAfter = scheduledAfter;
            ScheduledBefore = scheduledBefore;
            OrderBy = SetOrderBy(orderBy);
            IsDescendingOrder = isDescendingOrder;
            Search = search;
        }

        public string SiteId { get; }
        public Guid? DeviceId { get; }
        public string ModuleId { get; }
        public IEnumerable<string>? JobActions { get; }
        public int ItemsPerPage { get; }
        public int CurrentPage { get; }
        public DateTime? CreatedAfter { get; }
        public Guid? CreatedBy { get; }
        public IEnumerable<JobStatusType>? JobStatuses { get; }
        public string JobName { get; }
        public IEnumerable<string>? DeviceTypes { get; }
        public DateTime? ScheduledAfter { get; }
        public DateTime? ScheduledBefore { get; }
        public string OrderBy { get; }
        public bool IsDescendingOrder { get; }
        public string Search { get; }

        private string SetOrderBy(string orderByFromRequest)
        {
            switch (orderByFromRequest)
            {
                case "moduleId":
                    return "jobAction.moduleId";
                case "jobAction":
                    return "jobAction.name";
                case "jobName":
                    return "name";

                case "createdAt":
                case "scheduled":
                case "jobStatus":
                    return orderByFromRequest;
                default:
                    return "createdAt";
            }
        }

        public static JobFilter Create() 
        {
            return new JobFilter(
                    null,
                    null,
                    null,
                    null,
                    20,
                    1,
                    null,
                    null,
                    new List<JobStatusType>(),
                    null,
                    new List<string>(),
                    null, null, null, true, null);
        }
    }
}
