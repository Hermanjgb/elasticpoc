﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticSearchFunctionPOC.Ports.Dto
{
    public class InvocationDetailsDto
    {
        [JsonProperty(PropertyName = "methodDefinition")]
        public ManifestMethodDefinitionDto MethodDefinition { get; }

        [JsonProperty(PropertyName = "payload")]
        public IList<KeyValuePair<string, string>> Payload { get; }

        [JsonProperty(PropertyName = "synchronousStatusUpdate")]
        public StatusUpdate? SynchronousStatusUpdate { get; }

        [JsonProperty(PropertyName = "correlationContext")]
        public CorrelationContext CorrelationContext { get; }

        [JsonConstructor]
        public InvocationDetailsDto(
            ManifestMethodDefinitionDto methodDefinition,
            IList<KeyValuePair<string, string>> payload,
            StatusUpdate? synchronousStatusUpdate,
            CorrelationContext correlationContext)
        {
            MethodDefinition = methodDefinition;
            Payload = payload;
            SynchronousStatusUpdate = synchronousStatusUpdate;
            CorrelationContext = correlationContext;
        }
    }
}
