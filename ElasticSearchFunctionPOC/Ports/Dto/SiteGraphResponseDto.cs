﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticSearchFunctionPOC.Ports.Dto
{
    public class SiteGraphResponseDto
    {
        [JsonProperty("data", NullValueHandling = NullValueHandling.Ignore)]
        public DataDto? Data { get; set; }

        [JsonProperty("errors", NullValueHandling = NullValueHandling.Ignore)]
        public IEnumerable<ErrorsDto> Errors { get; set; }
    }
}
