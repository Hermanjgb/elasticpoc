﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticSearchFunctionPOC.Ports.Dto
{
    public enum Providers
    {
        CorrelationId,
        MacAddress,
        UploadPath,
        DownloadPath,
        ApiKey,
        List
    }
}
