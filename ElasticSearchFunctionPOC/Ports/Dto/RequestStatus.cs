﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticSearchFunctionPOC.Ports.Dto
{
    public enum RequestStatus
    {
        Acknowledged,
        Running,
        Completed,
        Failed,
        Cancelled,
        CannotCancel
    }
}
