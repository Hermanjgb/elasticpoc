﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticSearchFunctionPOC.Ports.Dto
{
    public class JobActionDto
    {
        [JsonProperty("name")]
        public string Name { get; }

        [JsonProperty("moduleId")]
        public string ModuleId { get; }

        [JsonProperty("edgeId")]
        public string EdgeId { get; }

        [JsonProperty("fields")]
        public IEnumerable<ActionFieldDto> Fields { get; }

        public JobActionDto(string name, string edgeId, string moduleId, IEnumerable<ActionFieldDto> fields)
        {
            Name = name;
            EdgeId = edgeId;
            ModuleId = moduleId;
            Fields = fields;
        }

        public JobActionDto WithFields(List<ActionFieldDto> newFields)
        {
            return new JobActionDto(Name, EdgeId, ModuleId, newFields);
        }
    }
}
