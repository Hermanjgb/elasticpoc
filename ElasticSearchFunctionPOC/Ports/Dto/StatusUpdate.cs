﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;

namespace ElasticSearchFunctionPOC.Ports.Dto
{
    public class StatusUpdate
    {
        [JsonConstructor]
        public StatusUpdate(
            [JsonConverter(typeof(StringEnumConverter))] RequestStatus status,
            string? message,
            JObject? payload)
        {
            Status = status;
            Message = message;
            Payload = payload;
        }

        [JsonConverter(typeof(StringEnumConverter))]
        public RequestStatus Status { get; }

        public string? Message { get; }

        [JsonProperty("payload", NullValueHandling = NullValueHandling.Ignore)]
        public JObject? Payload { get; }
    }
}
