﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticSearchFunctionPOC.Ports.Dto
{
    public class JobDataDto
    {
        [JsonProperty("name")]
        public string Name { get; }

        [JsonProperty("note")]
        public string Note { get; }

        [JsonProperty("scheduled")]
        public ScheduleTimeDto? Scheduled { get; set; }

        [JsonProperty("userGroups")]
        public IEnumerable<Guid> UserGroups { get; }

        [JsonProperty("action")]
        public JobActionDto Action { get; }

        [JsonConstructor]
        public JobDataDto(
            string name,
            string note,
            ScheduleTimeDto? scheduled,
            IEnumerable<Guid> userGroups,
            JobActionDto action)
        {
            Name = name;
            Note = note;
            Scheduled = scheduled;
            UserGroups = userGroups;
            Action = action;
        }
    }
}
