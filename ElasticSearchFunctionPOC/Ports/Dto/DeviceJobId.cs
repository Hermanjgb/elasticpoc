﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticSearchFunctionPOC.Ports.Dto
{
    public class DeviceJobId
    {
        [JsonProperty("jobId")]
        public Guid JobId { get; }

        [JsonProperty("deviceId")]
        public Guid DeviceId { get; }

        [JsonConstructor]
        public DeviceJobId(Guid jobId, Guid deviceId)
        {
            JobId = jobId;
            DeviceId = deviceId;
        }

        private bool Equals(DeviceJobId other)
        {
            return JobId.Equals(other.JobId) && DeviceId.Equals(other.DeviceId);
        }

        public override bool Equals(object? obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((DeviceJobId)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(JobId, DeviceId);
        }

        public static bool operator ==(DeviceJobId? left, DeviceJobId? right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(DeviceJobId? left, DeviceJobId? right)
        {
            return !Equals(left, right);
        }
    }
}
