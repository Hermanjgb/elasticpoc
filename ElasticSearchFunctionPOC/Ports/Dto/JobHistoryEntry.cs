﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticSearchFunctionPOC.Ports.Dto
{
    public class JobHistoryEntry
    {
        [JsonProperty("action")]
        [JsonConverter(typeof(StringEnumConverter))]
        public JobActionType Action { get; set; }

        [JsonProperty("timestamp")]
        public DateTime Timestamp { get; set; }

        [JsonProperty("user")]
        public Guid User { get; set; }
    }
}
