﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticSearchFunctionPOC.Ports.Dto
{
    public class ManifestMethodDefinitionDto
    {
        [JsonProperty("id")]
        public string? Id { get; set; }

        [JsonProperty("description", NullValueHandling = NullValueHandling.Ignore)]
        public string? Description { get; set; }

        [JsonProperty("methodName")]
        public string? MethodName { get; set; }

        [JsonProperty("scope")]
        public string? Scope { get; set; }

        [JsonProperty("fields")]
        public IEnumerable<ManifestActionFieldDto>? Fields { get; set; }

        [JsonProperty("timeout")]
        public int? Timeout { get; set; }
    }
}
