﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticSearchFunctionPOC.Ports.Dto
{
    public enum JobStatusType
    {
        New = 0,
        Scheduled = 1,
        Running = 2,
        Failed = 3,
        Cancelled = 4,
        Completed = 5,
        CompletedWithErrors = 6,
        Cancelling = 7,
        CancellationPending = 8
    }
}
