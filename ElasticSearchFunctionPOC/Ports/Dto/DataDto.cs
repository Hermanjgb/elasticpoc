﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticSearchFunctionPOC.Ports.Dto
{
    public class DataDto
    {
        [JsonProperty("sites")]
        public IEnumerable<SiteDto> Sites { get; set; }

        public DataDto(IEnumerable<SiteDto> sites)
        {
            Sites = sites;
        }
    }
}
