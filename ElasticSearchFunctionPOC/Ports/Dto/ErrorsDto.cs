﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticSearchFunctionPOC.Ports.Dto
{
    public class ErrorsDto
    {
        [JsonProperty("message")]
        public string? Message { get; set; }

        [JsonProperty("locations")]
        public IEnumerable<LocationsDto>? Locations { get; set; }
    }
}
