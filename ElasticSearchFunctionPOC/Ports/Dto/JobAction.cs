﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticSearchFunctionPOC.Ports.Dto
{
    public class JobAction
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("moduleId")]
        public string ModuleId { get; set; }

        [JsonProperty("edgeId")]
        public string EdgeId { get; set; }

        [JsonProperty("fields")]
        public IEnumerable<JobActionField> Fields { get; set; }
    }
}
