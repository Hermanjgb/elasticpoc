﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticSearchFunctionPOC.Ports.Dto
{
    public class JobStatusWithPagingDto<TSummaryType>
    {
        public JobStatusWithPagingDto(
            IEnumerable<JobDto> jobs,
            int totalCount,
            int currentPage,
            int itemsPerPage)
        {
            Jobs = jobs;
            TotalCount = totalCount;
            CurrentPage = currentPage;
            ItemsPerPage = itemsPerPage;
        }

        [JsonProperty("jobs")]
        public IEnumerable<JobDto> Jobs { get; }

        [JsonProperty("totalCount")]
        public int TotalCount { get; }

        [JsonProperty("currentPage")]
        public int CurrentPage { get; set; }

        [JsonProperty("itemsPerPage")]
        public int ItemsPerPage { get; set; }
    }
}
