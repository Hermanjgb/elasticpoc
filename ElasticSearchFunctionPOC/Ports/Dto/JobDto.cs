﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Text;

namespace ElasticSearchFunctionPOC.Ports.Dto
{
    public class JobDto : JobDataDto
    {
        public JobDto(Guid jobId,
            string name,
            string note,
            DateTime createdAt,
            Guid createdBy,
            ScheduleTimeDto? scheduledFor,
            JobStatusType jobOverallStatus,
            JobActionDto action,
            IEnumerable<DetailedJobSummaryDto> siteStatues,
            IEnumerable<Guid> userGroups,
            IEnumerable<string> deviceTypes,
            IEnumerable<JobHistoryEntry> history)
        : base(name, note, scheduledFor, userGroups, action)
        {
            JobId = jobId;
            CreatedAt = createdAt;
            CreatedBy = createdBy;
            Statuses = siteStatues;
            OverallStatus = jobOverallStatus;
            DeviceTypes = deviceTypes;
            History = history;
        }

        [JsonProperty("jobId")]
        public Guid JobId { get; }

        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; }

        [JsonProperty("createdBy")]
        public Guid CreatedBy { get; }

        [JsonProperty("status")]
        [JsonConverter(typeof(StringEnumConverter))]
        public JobStatusType OverallStatus { get; }

        [JsonProperty("history")]
        public IEnumerable<JobHistoryEntry> History { get; }

        [JsonProperty("statuses")]
        public IEnumerable<DetailedJobSummaryDto> Statuses { get; }

        [JsonProperty("deviceTypes")]
        public IEnumerable<string> DeviceTypes { get; }
    }
}
