﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchFunctionPOC.Ports
{
    public interface ITokenProvider
    {
        Task<string> GetAccessToken();
    }
}
