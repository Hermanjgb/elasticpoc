﻿using ElasticSearchFunctionPOC.Ports.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ElasticSearchFunctionPOC.Ports
{
    public interface IJobStatusRepository
    {
        Task<IEnumerable<SiteJobStatus>> GetPagedSitesJobsStatusesFilteredByAsync(string siteId, Guid? deviceId, DateTime? createdAfter, CancellationToken cancellationToken);
        Task<IEnumerable<JobStatus>> GetJobStatusAsync(IEnumerable<Guid> jobIds, CancellationToken cancellationToken);
        Task<IEnumerable<JobStatus>> GetJobStatusAsync(IEnumerable<Guid> jobIds, string moduleId, IEnumerable<string>? jobActions,
                                                        DateTime? scheduledAfter, DateTime? scheduledBefore,
                                                        Guid? createdBy, string jobName, IEnumerable<string>? deviceTypes,
                                                        string orderBy, bool isDesc, int elementsPerPage, int currentPage,
                                                        string searchText, IEnumerable<Guid>? authorizedUserGroups, CancellationToken cancellationToken);
        Task<int> CountJobStatusAsync(IEnumerable<Guid> jobIds, string moduleId, IEnumerable<string>? jobActions,
                                                        DateTime? scheduledAfter, DateTime? scheduledBefore,
                                                        Guid? createdBy, string jobName, IEnumerable<string>? deviceTypes,
                                                        string searchText, IEnumerable<Guid>? authorizedUserGroups, CancellationToken cancellationToken);
    }
}
